EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_RaspberryPi_and_Boards:Pico U?
U 1 1 63DA369A
P 2550 3500
F 0 "U?" H 2550 4715 50  0000 C CNN
F 1 "Pico" H 2550 4624 50  0000 C CNN
F 2 "RPi_Pico:RPi_Pico_SMD_TH" V 2550 3500 50  0001 C CNN
F 3 "" H 2550 3500 50  0001 C CNN
	1    2550 3500
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA356xxDBV U?
U 1 1 63DA52CE
P 4950 1750
F 0 "U?" H 5294 1796 50  0000 L CNN
F 1 "OPA356xxDBV" H 5294 1705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4850 1550 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa356.pdf" H 4950 1950 50  0001 C CNN
	1    4950 1750
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA355NA U?
U 1 1 63DA842A
P 7300 1850
F 0 "U?" H 7644 1896 50  0000 L CNN
F 1 "OPA355NA" H 7644 1805 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 7450 1600 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa355.pdf" H 7450 2000 50  0001 C CNN
	1    7300 1850
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA355NA U?
U 1 1 63DA8F1A
P 8750 1950
F 0 "U?" H 9094 1996 50  0000 L CNN
F 1 "OPA355NA" H 9094 1905 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 8900 1700 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa355.pdf" H 8900 2100 50  0001 C CNN
	1    8750 1950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
